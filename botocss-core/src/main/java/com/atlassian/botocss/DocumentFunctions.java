package com.atlassian.botocss;

import java.util.function.Function;

import org.jsoup.nodes.Document;

/**
 * Stateless functions which can be used to modify the JSoup document or its
 * serialization.
 * <p/>
 * Note that not all functions are composeable, as the might set conflicting
 * changes. For example, {@link #PRETTY_PRINT} sets the whitespace indent to
 * four and {@link #ZERO_INDENT} sets it to zero.
 *
 * @see Botocss#inject(String, BotocssStyles, Function)
 */
public enum DocumentFunctions implements Function<Document, Document> {
    NOOP(),

    /**
     * Sets the output settings to pretty print and to indent by four whitespaces.
     */
    PRETTY_PRINT(document -> {
        document.outputSettings().prettyPrint(true);
        document.outputSettings().indentAmount(4);
        return null;
    }),

    /**
     * Sets the output settings to zero indent, the default is one whitespace.
     */
    ZERO_INDENT(document -> {
        document.outputSettings().indentAmount(0);
        return null;
    });

    private Function<Document, Document> delegate = Function.identity();

    DocumentFunctions() {}

    DocumentFunctions(Function<Document, Document> delegate) {
        this.delegate = delegate;
    }

    @Override
    public Document apply(Document document) {
        return delegate.apply(document);
    }
}
