package com.atlassian.botocss;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Selector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unbescape.css.CssEscape;

import cz.vutbr.web.css.CombinedSelector;
import cz.vutbr.web.css.Declaration;
import cz.vutbr.web.css.RuleBlock;
import cz.vutbr.web.css.RuleSet;
import cz.vutbr.web.css.StyleSheet;
import cz.vutbr.web.css.Term;
import cz.vutbr.web.css.TermColor;
import cz.vutbr.web.css.TermLength;
import cz.vutbr.web.css.TermURI;
import cz.vutbr.web.csskit.Color;

import static java.util.Collections.emptyList;

/**
 * Public facade for Botocss. Use {@link #inject(String, String...)} as a simple method
 * of injecting CSS stylesheets into an HTML document.
 */
public final class Botocss {
    private static final int INCH_MULTIPLIER = 96;
    private static final float PC_MULTIPLIER = INCH_MULTIPLIER / 6f;
    private static final float MM_MULTIPLIER = INCH_MULTIPLIER / 25.4f;
    private static final float CM_MULTIPLIER = INCH_MULTIPLIER / 2.54f;
    private static final float PT_MULTIPLIER = INCH_MULTIPLIER / 72f;

    private static final Logger log = LoggerFactory.getLogger(Botocss.class);

    private static final Map<String, BotocssExpansion> EXPANDABLE_ATTRS = new HashMap<>();

    static {
        EXPANDABLE_ATTRS.put("width", new BotocssExpansion("width", Botocss::parseLengthForExpansion));
        EXPANDABLE_ATTRS.put("height", new BotocssExpansion("height", Botocss::parseLengthForExpansion));
        EXPANDABLE_ATTRS.put("background-color", new BotocssExpansion("bgcolor", Botocss::parseColorForExpansion));
    }

    /**
     * Injects the CSS from the stylesheets into the provided HTML as inline styles.
     * Also applies any inline &lt;style&gt; elements found in the HTML content.
     * <p/>
     * See the Botocss home page for documentation and caveats around the
     * injection process.
     *
     * @param html        the HTML document
     * @param stylesheets the stylesheets containing the CSS to inject
     * @return the injected HTML string
     * @throws NullPointerException if the provided HTML or any of the stylesheets are {@code null}
     */
    public static String inject(String html, String... stylesheets) {
        log.debug("Parsing external stylesheets");
        BotocssStyles styles = BotocssStyles.parse(stylesheets);
        return inject(html, styles);
    }

    /**
     * Injects parsed stylesheets into the provided HTML as inline styles.
     * Also applies any inline &lt;style&gt; elements found in the HTML content.
     * Use {@link #parse} to create a stylesheet object.
     * <p/>
     * See the Botocss home page for documentation and caveats around the
     * injection process.
     *
     * @param html   the HTML document
     * @param styles the pre-parsed stylesheets containing the CSS to inject
     * @return the injected HTML string
     * @throws NullPointerException if the provided HTML or styles are {@code null}
     * @see #parse
     */
    public static String inject(String html, BotocssStyles styles) {
        return inject(html, styles, DocumentFunctions.PRETTY_PRINT);
    }

    /**
     * Injects parsed stylesheets into the provided HTML as inline styles.
     * Also applies any inline &lt;style&gt; elements found in the HTML content.
     * Use {@link #parse} to create a stylesheet object.
     * <p/>
     * See the Botocss home page for documentation and caveats around the
     * injection process.
     *
     * @param html             the HTML document
     * @param styles           the pre-parsed stylesheets containing the CSS to inject
     * @param documentFunction modify the parsed HTML document after injection before serializing
     * @return the injected HTML string
     * @throws NullPointerException if the provided HTML or styles are {@code null}
     * @see #parse
     */
    public static String inject(String html, BotocssStyles styles, Function<Document, Document> documentFunction) {
        return inject(html, styles, documentFunction, true);
    }

    /**
     * Injects parsed stylesheets into the provided HTML as inline styles.
     * Also applies any inline &lt;style&gt; elements found in the HTML content.
     * Use {@link #parse} to create a stylesheet object.
     * <p/>
     * See the Botocss home page for documentation and caveats around the
     * injection process.
     *
     * @param html                the HTML document
     * @param styles              the pre-parsed stylesheets containing the CSS to inject
     * @param documentFunction    modify the parsed HTML document after injection before serializing
     * @param processInlineStyles whether to process inline styles. If inline styles are only used for media query CSS
     *                            styles, processing them is an unnecessary performance reduction.
     * @return the injected HTML string
     * @throws NullPointerException if the provided HTML or styles are {@code null}
     * @see #parse
     */
    public static String inject(
            String html,
            BotocssStyles styles,
            Function<Document, Document> documentFunction,
            boolean processInlineStyles) {
        final Map<String, String> stringCache = new HashMap<>();
        final Function<String, String> cache = k -> stringCache.computeIfAbsent(k, fKey -> k);

        long start = System.currentTimeMillis();

        final Document documentWithCSSInlined = Jsoup.parse(html);
        log.debug("Parsed HTML document in {} ms", System.currentTimeMillis() - start);

        int selectorCount = 0;

        log.debug("Applying external stylesheets");
        selectorCount += applyStyles(cache, documentWithCSSInlined, styles);

        if (processInlineStyles) {
            log.debug("Finding inline stylesheets");
            final Set<String> inlineCssStyles = new LinkedHashSet<>();
            for (Element inlineStyle : documentWithCSSInlined.getElementsByTag("style")) {
                inlineCssStyles.add(inlineStyle.html());
            }

            if (!inlineCssStyles.isEmpty()) {
                log.debug("Parsing inline stylesheets");
                BotocssStyles inlineStyles = BotocssStyles.parse(inlineCssStyles.toArray(new String[0]));

                log.debug("Applying inline stylesheets");
                selectorCount += applyStyles(cache, documentWithCSSInlined, inlineStyles);
            }
        }

        final Document processedDocumentWithCSSInlined = documentFunction.apply(documentWithCSSInlined);
        final Document resultDocumentWithCSSInlined = processedDocumentWithCSSInlined != null
                ? processedDocumentWithCSSInlined
                : documentWithCSSInlined; // default to the given document

        final String result = resultDocumentWithCSSInlined.outerHtml();

        log.info(
                "Applying {} CSS selectors to HTML (length {}) took {} ms",
                selectorCount,
                html.length(),
                System.currentTimeMillis() - start);
        return result;
    }

    /**
     * Parses the provided CSS and returns a list of pre-parsed Stylesheet objects for
     * use with {@link #inject(String, BotocssStyles)}.
     *
     * @param stylesheets the CSS stylesheets to parse
     * @throws NullPointerException if any of the provided stylesheets are {@code null}
     * @see #inject(String, BotocssStyles)
     */
    public static BotocssStyles parse(String... stylesheets) {
        return BotocssStyles.parse(stylesheets);
    }

    private static int applyStyles(Function<String, String> cache, Document document, BotocssStyles styles) {
        int selectorCount = 0;
        for (StyleSheet styleSheet : styles.getStyleSheets()) {
            selectorCount += applyStylesheet(cache, document, styleSheet);
        }
        return selectorCount;
    }

    private static int applyStylesheet(Function<String, String> cache, Document document, StyleSheet stylesheet) {
        int selectorCount = 0;

        for (RuleBlock<?> block : stylesheet) {
            if (!(block instanceof RuleSet)) continue;

            final RuleSet set = (RuleSet) block;
            for (CombinedSelector selector : set.getSelectors()) {
                selectorCount++;
                log.debug("Applying selector #{}: {}", selectorCount, selector.toString());
                final List<Element> elements = findElements(document, selector);
                for (Element element : elements) {
                    for (Declaration declaration : set) {
                        log.debug(
                                "Applying style [ {} ] to element: {}",
                                declaration.toString().trim(),
                                element.nodeName());
                        try {
                            final String ruleKey = declaration.getProperty() + ": " + getStringValue(declaration);
                            final String ruleVal = cache.apply(ruleKey);

                            final String existingStyleKey = element.attr("style");
                            final String existingStyleVal = cache.apply(existingStyleKey);

                            final String updateStyleKey =
                                    existingStyleVal.equals("") ? ruleVal : existingStyleVal + "; " + ruleVal;
                            final String updateStyleVal = cache.apply(updateStyleKey);

                            element.attr("style", updateStyleVal);

                            expandProperties(cache, element, declaration);
                        } catch (IllegalArgumentException e) {
                            log.warn("Failed to process CSS property value: " + e.getMessage());
                        }
                    }
                }
            }
        }

        return selectorCount;
    }

    private static List<Element> findElements(Document document, CombinedSelector selector) {
        try {
            // Need to unescape the selector because jsoup doesn't support CSS-escaped selectors
            // https://github.com/jhy/jsoup/issues/577
            return document.select(CssEscape.unescapeCss(selector.toString()));
        } catch (Selector.SelectorParseException e) {
            log.info("Skipping unsupported selector: " + selector.toString());
            return emptyList();
        }
    }

    private static String getStringValue(Declaration declaration) {
        StringBuilder result = new StringBuilder();
        for (Term<?> term : declaration) {
            result.append(getStringValue(term));
        }
        return result.toString();
    }

    private static String getStringValue(Term<?> term) {
        // jStyeParser over-escapes the url value so we put it as is
        if (term instanceof TermURI) {
            final String operator =
                    term.getOperator() == null ? "" : term.getOperator().value();
            return operator + "url(" + term.getValue() + ")";
        }
        return term.toString();
    }

    private static String formatColor(TermColor term) {
        final Color color = term.getValue();
        return String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
    }

    private static String formatNumber(float value) {
        if (value == Math.ceil(value)) return String.valueOf(Math.round(value));
        else return String.format(Locale.ENGLISH, "%.3f", value);
    }

    /* Expansions */

    private static void expandProperties(Function<String, String> cache, Element element, Declaration declaration) {
        // Expand into deprecated attributes
        final String property = declaration.getProperty();

        if (declaration.isEmpty()) {
            log.debug("Value for {} not provided", property);
            return;
        }

        final BotocssExpansion expansion = EXPANDABLE_ATTRS.get(property);

        if (expansion == null) {
            return;
        }

        final String value = expansion.getProcessor().parse(declaration);

        if (value == null) {
            return;
        }

        element.attr(cache.apply(expansion.getAttributeName()), cache.apply(value));
    }

    private static String parseLengthForExpansion(Declaration declaration) {
        final Term<?> term = declaration.get(0);
        if (!(term instanceof TermLength)) {
            // This happens when we have a percentage
            log.debug("Not converting length for {}", declaration);
            return null;
        }
        TermLength termLength = (TermLength) term;
        final float multiplier;
        switch (termLength.getUnit()) {
            case px:
                multiplier = 1;
                break;
            case in:
                multiplier = INCH_MULTIPLIER;
                break;
            case pt:
                multiplier = PT_MULTIPLIER;
                break;
            case cm:
                multiplier = CM_MULTIPLIER;
                break;
            case mm:
                multiplier = MM_MULTIPLIER;
                break;
            case pc:
                multiplier = PC_MULTIPLIER;
                break;
            default:
                multiplier = 0;
                log.debug("Not converting length for {}", declaration);
                break;
        }

        return (multiplier > 0) ? formatNumber(termLength.getValue() * multiplier) : null;
    }

    private static String parseColorForExpansion(Declaration declaration) {
        final Term<?> term = declaration.get(0);
        if (!(term instanceof TermColor)) {
            log.debug("Not converting color for {}", declaration);
            return null;
        }
        return formatColor((TermColor) term);
    }
}
