package com.atlassian.botocss;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToCompressingWhiteSpace;

import static com.atlassian.botocss.Classpath.getResource;

@RunWith(Parameterized.class)
public class InjectionTest {
    private final String fileName;

    public InjectionTest(String fileName) {
        this.fileName = fileName;
    }

    @Parameterized.Parameters(name = "{index} : {0}")
    public static List<Object[]> data() {
        Object[][] data = {
            {"simple"},
            {"margin"},
            {"confluence"},
            {"jira-issues"},
            {"selectors"},
            {"attribute-selectors"},
            {"inline-styles"},
            {"pseudo"},
            {"escaping"},
            {"units"},
            {"recommended"},
        };
        return Arrays.asList(data);
    }

    @Test
    public void testStyles() {
        String html = getResource(fileName + ".html");
        String css = getResource(fileName + ".css");
        String output = css == null ? Botocss.inject(html) : Botocss.inject(html, css);
        final String expected = getResource(fileName + "-output.html");
        assertThat(fileName, output, equalToCompressingWhiteSpace(Objects.requireNonNull(expected)));
    }
}
